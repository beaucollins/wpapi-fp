<?php
namespace WPFP;

use WP_Error;
use WP_REST_Request;
use WP_REST_Response;

/**
 * @template R
 * @param Closure(WP_REST_Request):(R|WP_Error) $resource_fetcher
 * @param Closure(WP_REST_Request, R):(WP_REST_Response|WP_Error) $resource_handler
 * @return Closure(WP_REST_Request):WP_REST_Response
 */
function with_resource( $resource_fetcher, $resource_handler ) {
		return
		/**
		 * @param WP_REST_Request $request
		 * @return WP_REST_Response
		 */
		function( $request ) use ( $resource_fetcher, $resource_handler ) {
				$resource = $resource_fetcher( $request );
				if ( $resource instanceof WP_Error ) {
					return error_as_response( $resource );
				}

				$response = $resource_handler( $request, $resource );
				if ( $response instanceof WP_Error ) {
					return error_as_response( $response );
				}
				return $response;
		};
}

/**
 * @param WP_Error $error
 * @return WP_REST_Response
 */
function error_as_response( $error ) {
	/**
	 * @var mixed
	 */
	$data = $error->get_error_data();
	if ( is_array( $data ) ) {
		/**
		 * @var mixed
		 */
		$status = $data['status'];
		if ( ! is_int( $status ) ) {
			$status = 500;
		}
	} else {
		$status = 500;
	}
	return new WP_REST_Response(
		json_encode( [ 'error' => $error->get_error_message() ] ),
		$status,
		[ 'content-type' => 'application/json' ]
	);
}
