<?php
namespace WPFP;

use WP_Error;
use WP_REST_Request;
use WP_REST_Response;

use WPFP\ResourceTest\Cat;

final class ResourceTest extends \PHPUnit\Framework\TestCase {

    public function testResourceFetch(): void {
        $endpoint = with_resource( Cat::fetch( 'mungojerrie' ), Cat::asJson() );
        $response = $endpoint( new WP_REST_Request() );

        $this->assertEquals( 200, $response->get_status() );
        $this->assertEquals( json_encode( new Cat( 'mungojerrie' ) ), $response->get_data() );
    }

    public function testResourceFetchError(): void {
        $endpoint = with_resource( Cat::notFound(), Cat::asJson() );

        $response = $endpoint( new WP_REST_Request() );

        $this->assertEquals( 404, $response->get_status() );
        $this->assertEquals( json_encode( [ 'error' => 'Cat not found' ] ), $response->get_data() );
    }

    public function testResourceHandlerError(): void {
        $endpoint = with_resource(
            Cat::fetch( 'rumpleteaser' ),
            function() {
                return new WP_Error( 'cats', 'Bad request' );
            }
        );
        $response = $endpoint( new WP_REST_Request() );

        $this->assertEquals( 500, $response->get_status() );
        $this->assertEquals( json_encode( [ 'error' => 'Bad request' ] ), $response->get_data() );
    }
}

namespace WPFP\ResourceTest;

use JsonSerializable;

use WP_REST_Request;
use WP_REST_Response;
use WP_Error;

final class Cat implements JsonSerializable {

    /**
     * @return Closure(WP_REST_Request):(Cat|WP_Error)
     */
    public static function fetch( string $name ) {
        /**
         * @return Cat
         */
        return function( WP_REST_Request $request ) use ( $name ) {
            return new Cat( $name );
        };
    }

    /**
     * @return Closure(WP_REST_Request):(Cat|WP_Error)
     */
    public static function notFound() {
        return function( WP_REST_Request $request ) {
            return new WP_Error( 'cats', 'Cat not found', [ 'status' => 404 ] );
        };
    }

    /**
     * @return Closure(WP_REST_Request, Cat):(WP_REST_Response|WP_Error)
     */
    public static function asJson() {
        return function( WP_REST_Request $request, Cat $cat ) {
            return new WP_REST_Response(
                json_encode( $cat ),
                200,
                [ 'Content-Type' => 'application/json' ]
            );
        };
    }

    /**
     * @var string
     */
    private $name;

    public function __construct( string $name ) {
        $this->name = $name;
    }

    public function jsonSerialize() {
        return [
            'type' => 'cat',
            'name' => $this->name,
        ];
    }
}
